window.cs=new content_script();
function LoadPage(url,callback){
    cs.Ajax.get(cs.getUrl(url),(html)=>{
        html=html.replace("[HttpHome]/",cs.getUrl("/"));
        html=html.replace("[HttpHome]",cs.getUrl("/"));
        cs.toDocument(html).forEach((e)=>{
            if(e && e.nodeType!==3){
                (document.body||document.documentElement).appendChild(e);
            }
        });
        callback(html);
    })
}
function initHtml(dataList){
    LoadPage("/page/lef.html",()=>{
        new Vue({
            el:"#lefApp",
            data:{
                list:dataList,
                index:1,
                pageNum:10,
                from:{
                    isTmall:false,
                    minXL:0,
                    maxXL:100
                },
                hide:false
            },
            computed:{
                pageCount(){
                    let ret=this.PageList.length/this.pageNum;
                    if(this.PageList.length%this.pageNum!==0){
                        ret+=1;
                    }
                    return ret;
                },
                PageList(){
                    let arr=[];
                    let _self=this;
                    console.log(_self.list);
                    this.list.forEach((e)=>{
                        //判断是在销量内
                        if(e.sellCount>=_self.from.minXL && e.sellCount<=_self.from.maxXL){
                            //判断是否显示天猫店铺
                            if(_self.from.isTmall===true){
                                arr.push(e);
                            }else if(e.ismall===0){
                                arr.push(e);
                            }
                        }
                    });
                    //正叙
                    for ( let i=0;i<arr.length-1;i++){
                        //每轮比较次数，次数=长度-1-此时的轮数
                        for (let j=0;j<arr.length-1-i;j++) {
                            if (arr[j].sellCount < arr[j + 1].sellCount) {
                                let temp = arr[j];
                                arr[j] = arr[j + 1];
                                arr[j + 1] = temp;
                            }
                        }
                    }
                    return arr;
                }
            }
        })
    })
}
window.onload=()=>{
    let url=location.href;
    if(url.includes("://uland.taobao.com/sem/tbsearch?")){
        //淘宝热卖
        let par=cs.HtmlDecode(decodeURIComponent(document.body.innerHTML.match(/keyword=[\S]*catId=/g)[0]));
        let obj=cs.UrlDecode(par);
        //获取参数
        let time=new Date().getTime();
        let tooken=cs.CookieDecode(document.cookie)["_m_h5_tk"].match(/[\S]*(?=(_))/g)[0];
        let appKey="12574478";
        //加密
        let skey=sign(tooken,time,appKey,obj);
        obj=JSON.stringify(obj);
        let url="https://h5api.m.taobao.com/h5/mtop.alimama.union.sem.landing.pc.items/1.0/?"+cs.UrlEncode({
            jsv:"2.4.0",
            appKey:appKey,
            t:time,
            sign:skey,
            api:"mtop.alimama.union.sem.landing.pc.items",
            v:"1.0",
            AntiCreep:true,
            dataType:"jsonp",
            type:"jsonp",
            ecode:0,
            data:obj
        });
        cs.Ajax.jsonp(url,(e)=>{
            let dataList=e.data.mainItems;
            console.log(dataList);
            //加载页面
            initHtml(dataList);
        })
    }
};
